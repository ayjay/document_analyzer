Future
======
 - TODO: implement missing function: run document analyzer for all documents by type
 - TODO: implement missing function: run specific document analyzer for all documents by type
 - TODO: Handle results with the same name of different Analyzers (e.g access by index is based on the name)

1.0.0 (2016-02-01)
==================

- Initial release
