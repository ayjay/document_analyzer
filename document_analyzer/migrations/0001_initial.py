# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0027_auto_20150824_0702'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analyzer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(unique=True, max_length=32, verbose_name='Label')),
                ('slug', models.SlugField(help_text='Do not use hypens, if you need spacing use underscores.', verbose_name='Slug')),
                ('type', models.CharField(max_length=100, verbose_name='Type')),
                ('parameter', models.CharField(max_length=100, verbose_name='Parameter')),
                ('document_version', models.ForeignKey(related_name='analyzers', verbose_name='Document', to='documents.DocumentVersion')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parameter', models.CharField(max_length=255, verbose_name='Tag')),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('analyzer', models.ForeignKey(related_name='analyzer', verbose_name='Analyser', to='document_analyzer.Analyzer')),
                ('document_version', models.ForeignKey(related_name='analyzerresult', verbose_name='Document version', to='documents.DocumentVersion')),
            ],
            options={
                'verbose_name': 'Document version Analyzer Result',
                'verbose_name_plural': 'Document versions Analyzer Result',
            },
            bases=(models.Model,),
        ),
    ]
