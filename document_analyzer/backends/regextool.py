from __future__ import unicode_literals

import logging
import re

logger = logging.getLogger(__name__)


class RegexTool(object):
    def execute(self, document_version, parameter):
        ps = document_version.pages
        result = []
        method, regex = parameter.split(';', 1)
        reobj = re.compile(regex)

        for p in ps.all():
            logger.debug("running regex {} on page {}.".format(regex, p))
            # logger.debug(p.ocr_content.content)

            for m in reobj.finditer(p.ocr_content.content):
                for n, v in m.groupdict().iteritems():
                    value = v.capitalize()
                    result.append((n, value))
                    logger.debug('regex match:{}={}'.format(n, v))
                    if method == 'first':
                        return result

        return result
